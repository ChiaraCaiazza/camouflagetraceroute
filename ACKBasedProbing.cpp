#include <arpa/inet.h>
#include <asm/types.h>
#include <netinet/ip_icmp.h>
#include <netinet/tcp.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <netdb.h>
#include <sys/time.h>
#include <sys/socket.h>
#include <errno.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <linux/netlink.h>
#include <csignal>
#include <iostream>

#include "ACKBasedProbing.h"
#include "netfilter/messageCodes.h"



struct sockaddr_nl netlink_src_addr, netlink_dest_addr;
struct nlmsghdr *nlh = NULL;
struct iovec iov;
struct msghdr msg;



void clearQueue(){
	struct traceroute* element = head;


	while(element != NULL){
		head = head -> next;
		free(element);
		element = head;
	}
}



void printQueue(){
	int lastPrintedTTL = 0;
	char printStr[200];
	memset(printStr, '\0', sizeof(char) *200);

	//printf("In printQueue\n");
	struct traceroute* element = head;
	while(element != NULL){
		if (lastPrintedTTL == element->hop)
		{
			element = element -> next;
			continue;
		}

		
		sprintf(printStr, "%d) %s  ", element->hop, element->IPRouter);
		if (hopTracking == HOP_TRACKING_ENABLED)
		{
			char dummy[10];
			memset(dummy, '\0', sizeof(char) * 10);

			sprintf(dummy, "[%d]", element->attempt);
			strcat(printStr, dummy);
		}
		if (element -> tempo >= 0)
		{
			char dummy[50];
			memset(dummy, '\0', sizeof(char) * 50);
			sprintf(dummy, "      Tempo: %f ms\n", element->tempo);
			strcat(printStr, dummy);
		}

	    while (lastPrintedTTL + 1 < element -> hop)
		{
			lastPrintedTTL++;
			if (hopTracking == HOP_TRACKING_ENABLED)
				printf("%d) * [%d]\n", lastPrintedTTL, MAX_ATTEMPT);
			else
				printf("%d) * \n", lastPrintedTTL);

		}
		printf("%s", printStr);
		lastPrintedTTL = element -> hop;
		
		element = element -> next;
	}


	while (lastPrintedTTL < lastTTL)
	{
		lastPrintedTTL++;
		if (hopTracking == HOP_TRACKING_ENABLED)
			printf("%d) * [%d]\n", lastPrintedTTL, MAX_ATTEMPT);
		else
			printf("%d) * \n", lastPrintedTTL);
	}
}



void insertQueue(int hopNum, char* address, double timestamp, int attempt){
	//printf ("\n\n\nIn InsertQueue\n");
	//std::cout<<hopNum<<" "<<address<<" "<<timestamp<<" "<<attempt<<std::endl;

	struct traceroute *newEl = (struct traceroute*)malloc(sizeof(struct traceroute)),
	                  *currEl = head,
					  *pastCurrEl = NULL;

	

	if(newEl == NULL){
        printf("Error: unallocated memory!\n");
        std::raise(SIGINT);
    }

	memset(&newEl->IPRouter, '\0' , sizeof(newEl->IPRouter));
    strncpy(newEl->IPRouter, address, strlen(address));
    newEl->hop = hopNum;
    newEl->attempt = attempt;
    newEl->tempo = timestamp;
    newEl->next = NULL;

    if(head == NULL)
	{
        //add the first element
        head = newEl;
		return;

    }
    
	while(currEl != NULL)
	{
		if (currEl->hop < newEl->hop || (currEl->hop == newEl->hop && currEl->attempt < newEl->attempt))
		{
			//the current value has lower hop # or equal hop # with lower attempt #
			if (currEl->next == NULL)
			{
				//inserimento in coda
				currEl->next = newEl;
				newEl->next = NULL;
				return;
			}
		}
		else
		{
			//inseriamo newEl prima di currEl
			if (currEl != head)
			{
				pastCurrEl->next = newEl;
				newEl->next = currEl;
				return;
			}
			else
			{
				//inserimeto in testa
				newEl->next = currEl;
				head = newEl;
				return;
			}

		}
		pastCurrEl = currEl;
		currEl = currEl->next;
		
		
	}
	
}



void handlerQUIT(int sig){

	char stopMsg[3];
	int ret;

	//finalizing
	if (pendingRcvdICMPPckt.size() > 0)
	{
		//printf("add them to the list\n");

		std::map<std::pair<uint32_t, uint16_t>, std::tuple<int, int, double>>::iterator itTtlSeqMap;
		std::map<std::pair<uint32_t, uint16_t>, std::pair<char*, double>>::iterator itPendingICMPPkt;

		for (itPendingICMPPkt = pendingRcvdICMPPckt.begin(); itPendingICMPPkt != pendingRcvdICMPPckt.end(); ++itPendingICMPPkt)
		{
			itTtlSeqMap = ttlSeqMap.find(std::make_pair(itPendingICMPPkt->first.first, itPendingICMPPkt->first.second));
			if (itTtlSeqMap != ttlSeqMap.end())
			{
				//void insertQueue(int hopNum, char* address, double timestamp, int attempt)   	
				insertQueue(std::get<0>(ttlSeqMap[std::make_pair(itPendingICMPPkt->first.first, itPendingICMPPkt->first.second)]),
				             itPendingICMPPkt->second.first,
				            (itPendingICMPPkt->second.second  - std::get<2>(ttlSeqMap[std::make_pair(itPendingICMPPkt->first.first, itPendingICMPPkt->first.second)])), 
						   	std::get<1>(ttlSeqMap[std::make_pair(itPendingICMPPkt->first.first, itPendingICMPPkt->first.second)]));
					
			}		
		}

	}
	//print out the results
    printQueue();
	std::cout<<"LastTTL "<<lastTTL<<std::endl
			 <<"LastAttemptNumber "<<lastAttemptNumber<<std::endl;
	clearQueue();

	//close the sockets
	printf("\n\nSocket closure....\n");
	close(sd);
	close(raw_sock);

	//stop targeting the web server
	ret = sprintf(stopMsg, "%d", STOP_CODE);
	if (ret < 0)
	{
		printf ("errore in sprintf()\n");
	}
	strcat(stopMsg, "\0");
	send_kernel_message(stopMsg);
	receiveOKCode();

	//close the communication socket with the server
    close(netlink_sd);
	exit(0);
}



void handlerPipe(int sig){
    int ret;

    //ignoring new sigpipe in handlerPipe
	signal(SIGPIPE, SIG_IGN);

	inc = 0;
	sigpipeOccurred = 1;

    printf("\n\nSIGPIPE detected\n\n");

    //close the socket
	close(sd);

	if (reconnectOption == RECONNECT_DISABLED)
	{
	    //now -c option is not used. Clean the environment before opening a new connection.
        clearQueue();
	    actualTTL = 0;

        //options upgrading
		if (keepAliveOption == KEEP_ALIVE_DISABLED)
		{
			printf("\n- - - - - - - - - - - - - - - - - \nBroken stream, retry with -k option\n");
			keepAliveOption = KEEP_ALIVE_ENABLED;
		}
		else if (keepAliveOption == KEEP_ALIVE_ENABLED)
		{
			printf("\n- - - - - - - - - - - - - - - - - \nBroken stream, retry with -k -c options\n");
			reconnectOption = RECONNECT_ENABLED;
		}
	}
	else
	    //now -c option is used. The new connection starts from the last TTL
		--actualTTL;

    //establish a new connection
    ret = openingConnection();
    if (ret < 0)
        std::raise(SIGINT);

    //enabling again sigpipe catching
	signal(SIGPIPE, handlerPipe);

	return;
}



int openingConnection(){
    int ret,
		residualBindingTime = 5*60; //we try to bind a source port for at most 5 minutes

    //open the socket (TCP)
    sd = socket(AF_INET, SOCK_STREAM , 0);
	if(sd < 0){
		printf("Error in socket creation\n");

		return -1;
	}
	if (source_port != 0)
	{
		memset(&my_addr, 0, sizeof(my_addr));
		my_addr.sin_family = AF_INET;
		my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
		my_addr.sin_port = htons(source_port);

		while(1)
		{
			ret = bind(sd, (struct sockaddr*) &my_addr, sizeof(my_addr));
			if (ret < 0)
			{
				if (errno == EADDRINUSE)
				{
					residualBindingTime -= 10;
					if (residualBindingTime < 0)
					{
						printf("Too many bind attempts... EXITING\n");
						exit(0);
					}

					sleep(10);
					
					continue;
				}

				printf("Error in client bind()\n");
				printf("(%d)%s\n", errno, strerror(errno));

				exit(0);
			}
			else 
				break;
		}
	}

	//connect with the server
    ret = connect(sd, (struct sockaddr*)&sv_addr, sizeof(sv_addr));
    if ((ret) == -1)
    {
        printf("Failed to connect to the server: %s\n", strerror(errno));

        return -1;
    }

	int sendBufferSize = 0,
	    rcvBufferSize = 0;
	socklen_t sendOptSize = sizeof(sendBufferSize),
	          rcvOptSize = sizeof(rcvBufferSize);
	getsockopt(sd, SOL_SOCKET, SO_SNDBUFFORCE , &sendBufferSize, &sendOptSize);
	getsockopt(sd, SOL_SOCKET, SO_RCVBUFFORCE , &rcvBufferSize, &rcvOptSize);

	return 1;
}



int processPacket(char* buff, int dim, int hopNum, double temp){
	unsigned short iphdlen,
		           iperrlen;
	struct ip* IPerr;
	struct ip* iphd = (struct ip*)buff;
	struct tcphdr* tcphd;
	struct timespec timespec;

	
	//pointer to the ICMP header
	iphdlen = iphd->ip_hl*4;
	struct icmphdr* icmp = (struct icmphdr*)(buff + iphdlen);


	if(icmp->type == 11 && icmp->code == 0){
		//printf("ICMP TIME_EXCEEDED PKT");
		//ICMP type == TIME_EXCEEDED

		//8 bytes = ICMP header length (4 bytes) + unused field (4 byte)
		//IPerr is the pointer to the Internet Header + 64 bits of
		//Original Data Datagram
		IPerr = (struct ip*)(buff + iphdlen + 8);

        //check the dest address in the original pkt
		if(strcmp(inet_ntoa(IPerr->ip_dst), inet_ntoa(sv_addr.sin_addr)) == 0){
			std::map<std::pair<uint32_t, uint16_t>, std::tuple<int, int, double>>::iterator it;

			iperrlen = IPerr->ip_hl*4;
			tcphd = (struct tcphdr*)(buff + iphdlen + 8 + iperrlen);

			
			clock_gettime(CLOCK_REALTIME, &timespec); 
			it = ttlSeqMap.find(std::make_pair(ntohl(tcphd->th_seq), ntohs(IPerr->ip_id)));

			if (it != ttlSeqMap.end())
			{

				//this is an ICMP time exc. packet for a known seq #       	
				insertQueue(std::get<0>(ttlSeqMap[std::make_pair(ntohl(tcphd->th_seq), ntohs(IPerr->ip_id))]),
				            inet_ntoa(iphd->ip_src),
				            ((double)timespec.tv_sec*1000+(double)timespec.tv_nsec/1000000 - std::get<2>(ttlSeqMap[std::make_pair(ntohl(tcphd->th_seq), ntohs(IPerr->ip_id))])), 
						   	std::get<1>(ttlSeqMap[std::make_pair(ntohl(tcphd->th_seq), ntohs(IPerr->ip_id))]));
			}
			else
			//I have received the ICMP but we have no info about the ttl
			{
				pendingRcvdICMPPckt[std::make_pair(ntohl(tcphd->th_seq), ntohs(IPerr->ip_id))] = 
				            std::make_pair(inet_ntoa(IPerr->ip_src),
							(double)timespec.tv_sec*1000+(double)timespec.tv_nsec/1000000);
			}
		
			return 1;
		}
	}

	return 0;
}


void receiveKernelHopInfo()
{
	int ret;
	struct timespec timespec;

	while(1)
		{
			char* buffer;
			buffer = (char*)malloc(sizeof(char) * 1024);
			memset(buffer, '\0', sizeof(char) * 1024);


			//wait for a couple ttl, seq #
			ret = receive_kernel_message(MSG_DONTWAIT, buffer);
			if (ret == -1)
			{
				free(buffer);
				//no more messages from the kernel
				break;
			}
		

			char code_c[3] = {buffer[0], buffer[1], '\0'};
			int code = atoi(code_c);
			int rcvttl = 0,
			    rcvAttemptNumber;
			double rcvMilliseconds = 0;//,
			       //userMs = 0;
			uint32_t rcvSeqNumber = 0;
			uint32_t rcvIPID = 0;

			switch (code)
			{
				case TARGET_TTL_CODE:
					memset(buffer, '\0', sizeof(char) * 1024);
					ret = receive_kernel_message(0, buffer);
					rcvttl = atoi(buffer);

					memset(buffer, '\0', sizeof(char) * 1024);
					ret = receive_kernel_message(0, buffer);
					rcvSeqNumber = atoi(buffer);

					memset(buffer, '\0', sizeof(char) * 1024);
					ret = receive_kernel_message(0, buffer);
					rcvAttemptNumber = atoi(buffer);

					memset(buffer, '\0', sizeof(char) * 1024);
					ret = receive_kernel_message(0, buffer);
					rcvMilliseconds = std::stod(buffer);

					memset(buffer, '\0', sizeof(char) * 1024);
					ret = receive_kernel_message(0, buffer);
					rcvIPID = std::stod(buffer);

					if (lastTTL < rcvttl)
					{
						lastTTL = rcvttl;
						lastAttemptNumber = rcvAttemptNumber;
					}
					if (lastTTL == rcvttl && lastAttemptNumber < rcvAttemptNumber)
						lastAttemptNumber = rcvAttemptNumber;

					clock_gettime(CLOCK_REALTIME, &timespec);    

					ttlSeqMap[std::make_pair(rcvSeqNumber, rcvIPID)] = std::make_tuple(rcvttl, rcvAttemptNumber, rcvMilliseconds);


					break;

				case STOP_CODE:
					std::cout<<"received STOP!"<<std::endl;
					sleep(waitTime);
					receiveKernelHopInfo();
					receiveICMPPackets();
					free(buffer);
					handlerQUIT(0);
					

				default: 
					printf("Error: unexpected message %s. Aborting... \n", buffer);
					free(buffer);
					std::raise(SIGINT);
			}

			free(buffer);
		}
}



int performPathInspection(){
	int inc = 0,
	    ret,
	    byte = 1;
	double before,
		   beforeWaitReply,
		   beforeStart;
	struct timeval timems,
				   twaitReply,
				   tStart;
	char receivedPacket[1024];



	gettimeofday(&twaitReply, NULL);
	gettimeofday(&tStart, NULL);
	beforeWaitReply = (double)twaitReply.tv_sec * 1000 + (double)twaitReply.tv_usec / 1000;
	beforeStart = (double)tStart.tv_sec * 1000 + (double)tStart.tv_usec / 1000;

	while(1){
        //prelevo il tempo da assegnare a before (number of milliseconds)
		gettimeofday(&timems, NULL);
		before = (double)timems.tv_sec*1000 + (double)timems.tv_usec/1000;

        //send the request
		if(keepAliveOption == KEEP_ALIVE_ENABLED)
		    //option -k is setted: for each pkt send 1 byte
			ret = send(sd, &request[(++inc)], byte, MSG_DONTWAIT);
		else
		    //option -k is not setted: for each pkt send the whole request
			ret = send(sd, request, strlen(request), MSG_DONTWAIT);


		if (sigpipeOccurred)
		{
			sigpipeOccurred = 0;
			continue;
		}

        //wait for a response from the TCP connection
        while(1)
        {
			receiveKernelHopInfo();
			receiveICMPPackets();


            errno = 0;

            //wait for a packet to be received
            memset(&receivedPacket,0,sizeof(receivedPacket));
            ret=recvfrom(sd,receivedPacket,1024,MSG_DONTWAIT,NULL, NULL);

            if (ret == 0)
            {
                //the server closed this connection
			    close(sd);

                ret = openingConnection();
                if (ret < 0)
                    std::raise(SIGINT);

				gettimeofday(&twaitReply, NULL);
				beforeWaitReply = (double)twaitReply.tv_sec*1000 + (double)twaitReply.tv_usec/1000;

                break;
			}

			if (ret>0)
			{
				gettimeofday(&twaitReply, NULL);
				beforeWaitReply = (double)twaitReply.tv_sec*1000 + (double)twaitReply.tv_usec/1000;
			}

			if (ret < 0)
				break;
		}

		//if 0 messages have been received and connection is not restarted in the last 2 minutes -> returns
		gettimeofday(&twaitReply,NULL); 
		gettimeofday(&timems,NULL);        
		if(((double)twaitReply.tv_sec*1000+(double)twaitReply.tv_usec/1000) - beforeWaitReply > (2 * 60 * 1000))
		{
			std::cout<<"0 messages received in the last 2 minutes" << std::endl
					 << "break" << std::endl;
			
			break;
		}

		//if 20 minutes elapses from the first packets -> returns 
		if(((double)timems.tv_sec*1000+(double)timems.tv_usec/1000) - beforeStart > (20 * 60 * 1000))
		{
			std::cout<<"20 minutes elapsed -> application is blocked" << std::endl
					 << "break" << std::endl;
			return -1;
		}    

		//to bound the number of http req per second  
		if((((double)timems.tv_sec*1000+(double)timems.tv_usec/1000)-before) < 1000)
			sleep(1);  
	
    }

    return 1;
}



void receiveICMPPackets()
{
	int ret; 
	char receivedPacket[1024];

	//wait for an ICMP-TTL expired packet
	while(1)
	{            		
		//receive a pkt
		memset(&receivedPacket, '\0' , sizeof(receivedPacket));
		ret=recvfrom(raw_sock, receivedPacket, 1024, MSG_DONTWAIT, NULL, NULL);
		if(ret == -1)
			//there are no pending ICMP messages
			break;
		
		processPacket(receivedPacket, ret, actualTTL, 0);
	}
}



void handleParameters(int argc, char*argv[]){
    int ret;

    // getopt non stampa mess errore
	opterr = 0;

	printf("MAX_ATTEMPT = %d\n", MAX_ATTEMPT);

	while((ret = getopt(argc, argv, "kcwdanthls")) !=- 1)
		switch(ret){
			case 'k':
				printf("KEEPALIVE OPTION ENABLED\n");
				keepAliveOption = KEEP_ALIVE_ENABLED;
				break;

			case 'c':
				printf("RECONNECT OPTION ENABLED\n");
				reconnectOption = RECONNECT_ENABLED;
				break;

			case 't':
				printf("HOP TRACKING OPTION ENABLED\n");
				hopTracking = HOP_TRACKING_ENABLED;
				break;

			case 'w':
				waitTime = atoi(argv[optind]);

				if (waitTime > 10 || waitTime < 1)
				{
					printf("Error: The -w value has to be set in the [1, 10] range\n");
					exit(0);
				}

				printf("WAIT TIME = %i\n", waitTime);
				break;

			case 'a':
				strcpy(serverAddr, argv[optind]);
				printf("TARGET ADDRESS = %s\n", serverAddr);
				break;

			case 'd':
				strcpy(serverName, argv[optind]);
				printf("TARGET = %s\n", serverName);
				break;

			case 'n':
			    maxDepth = atoi(argv[optind]);
				printf("MAX_DEPTH = %d\n",  maxDepth);
				break;

			case 'l':
				lastTCPHop = atoi(argv[optind]);
				printf("lastTCPHop = %d\n",  lastTCPHop);
				break;

			case 'h':
				std::cout<<"Usage:"<<std::endl
				         <<"sudo ./TraceKernel [-c] [-k] [-w wait_time] [-s source_port] "
						 <<"[-a IPv4_target_address] [-d target_domain_name] "
						 <<"[-n max_hop_number] [-t] [-h] [-l minimum_hop_number]"
						 <<std::endl
						 <<"Options:" 
						 << std::endl
						 <<"-c\t\t\t\tUse the reconnect option"
						 <<std::endl
						 <<"-k\t\t\t\tSend one-byte length HTTP requests"
						 <<std::endl
						 <<"-w wait_time\t\t\tWait for an ICMP packet no more than wait_time seconds."
						 <<std::endl
						 <<"\t\t\t\tThe wait time option have to be set in the [1-10] interval."
						 <<std::endl
						 <<"-s source_port\t\t\tUse source_port value for outgoing packets."<<std::endl
						 <<"\t\t\t\tThe source_port has to be set in the [40000 - 60000] interval."
						 <<std::endl
						 <<"-a IPv4_target_address\t\tSet the target IPv4 address"
						 <<std::endl
						 <<"-d target_domain_name\t\tSet the target domain name"
						 <<std::endl
						 <<"-n max_hop_number\t\tSet the max TTL to be reached."
						 <<std::endl
						 <<"-t\t\t\t\tRecord the attempt number"
						 <<std::endl
						 <<"-l minimum_hop_number\t\tThe smallest hop number probed."
						 <<std::endl;

				exit(0);
				break;

			case 's':
				source_port = (uint16_t)atoi(argv[optind]);
				if (source_port > 60000 || source_port < 40000)
				{
					printf("Error: The -s value has to be set in the [40000 - 60000] range\n");

					exit(0);
				}
				printf("SOURCE_PORT = %d\n",  source_port);

				break;

			case '?':
				// Checks whether optopt is a printable character.
				if(isprint(optopt))
					printf("Unknown option '-%c'.\n", optopt);
				else
					printf("Unknown option character '\\x%x'.\n", optopt);
				exit(0);
			default:
				exit(0);
		}
}



int open_kernel_socket()
{
	struct timeval kernelTimer;
	kernelTimer.tv_sec = 60;
	kernelTimer.tv_usec = 0;

    netlink_sd = socket(PF_NETLINK, SOCK_RAW, NETLINK_USER);
    if (netlink_sd < 0)
        return -1;

	//my address
    memset(&netlink_src_addr, 0, sizeof(netlink_src_addr));
    netlink_src_addr.nl_family = AF_NETLINK;
	//self pid
    netlink_src_addr.nl_pid = getpid();

    bind(netlink_sd, (struct sockaddr *)&netlink_src_addr, sizeof(netlink_src_addr));

	//kernel address
    memset(&netlink_dest_addr, 0, sizeof(netlink_dest_addr));
    netlink_dest_addr.nl_family = AF_NETLINK;
	//For Linux Kernel
    netlink_dest_addr.nl_pid = 0; 
	//unicast
    netlink_dest_addr.nl_groups = 0;


	
	setsockopt(netlink_sd, SOL_SOCKET, SO_RCVTIMEO, (const char*)& kernelTimer, sizeof(kernelTimer));

    return netlink_sd;
}



int send_kernel_message(char* message)
{
    nlh = (struct nlmsghdr *)malloc(NLMSG_SPACE(MAX_NETLINK_PAYLOAD));
    memset(nlh, 0, NLMSG_SPACE(MAX_NETLINK_PAYLOAD));
    nlh->nlmsg_len = NLMSG_SPACE(MAX_NETLINK_PAYLOAD);
    nlh->nlmsg_pid = getpid();
    nlh->nlmsg_flags = 0;


    strcpy((char*)NLMSG_DATA(nlh), message);

    iov.iov_base = (void *)nlh;
    iov.iov_len = nlh->nlmsg_len;
    msg.msg_name = (void *)&netlink_dest_addr;
    msg.msg_namelen = sizeof(netlink_dest_addr);
    msg.msg_iov = &iov;
    msg.msg_iovlen = 1;

    //printf("Sending message to kernel: %s\n", message);
    sendmsg(netlink_sd, &msg, 0);

	free(nlh);

    
	return 0;
}



int receive_kernel_message(int flags, char* buffer)
{
	int ret;

    /* Read message from kernel and store in the buffer*/
	ret = recvmsg(netlink_sd, &msg, flags);

	if (flags == 0 && ret == -1 && (errno == ETIME || errno == EWOULDBLOCK || errno == EAGAIN))
	{
		std::cout<<"receive_kernel_message: timeout expired"
					<<std::endl;
		handlerQUIT(0);
	}

	if (buffer != NULL)
	{
		strcpy(buffer, (char*)NLMSG_DATA(nlh));
	}
	

	return ret;
}



int main(int argc, char*argv[]){
    int ret;
    char bufferHTTP[2056];


	signal(SIGINT, handlerQUIT);
	signal(SIGPIPE, handlerPipe);


	memset(&sv_addr, '\0', sizeof(sv_addr));
	memset(&serverAddr, '\0', sizeof(char) * strlen(serverAddr));
	memset(&serverName, '\0', sizeof(char) * strlen(serverName));
	memset(&bufferHTTP, '\0', sizeof(char) * strlen(bufferHTTP));


	handleParameters(argc, argv);


    //create the raw socket (for ICMP pkt)
	raw_sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if(raw_sock < 0){
		printf("Error in raw socket creation\n");

		std::raise(SIGINT);
	}

	//initialize the netlink socket (to exchange messages with camoFilter)
    int netlink_sd = open_kernel_socket();
    if (netlink_sd < 0)
    {
        printf("Error in open_kernel_socket\n");
		std::raise(SIGINT);
    }
	//send IP target, max attemp # and max ttl to camoFilter
	sendInitialParameter(serverAddr);
	//wait until the start command is received
	receiveStartCode();

	//initialize the struct sockaddr_in with the information about the server
	sv_addr.sin_family = AF_INET;
	sv_addr.sin_port = htons(80);
	inet_pton(AF_INET, serverAddr, &sv_addr.sin_addr);
	// connessione al server
	printf("\nConnecting to %s (%s) .....\n", serverName, serverAddr);
    ret = openingConnection();
	if (ret < 0)
	{
		//the connect() fails
		std::cout<<"Error in openingConnection()"<<std::endl;
		std::raise(SIGINT);
	}
	//we are successfulyy connected with the server
	printf("Connected\n\n");

	//create the http request
	memset(&request, '\0', sizeof(char) * strlen(request));
    sprintf(request, "GET / HTTP/1.1\r\nHost: %s\r\nConnection: keep-alive\r\n\r\n", serverName);

	//start to send http messages
	ret = performPathInspection();
	if (ret < 0)
	{
		printf("Error in performPathInspection()\nExiting\n");

		std::raise(SIGINT);
	}

	//exiting
    handlerQUIT(0);

	return 1;
}



void sendInitialParameter(char* IP)
{
	char msg[1024],
		 s[20];
	int ret;
	
	
	//initialize the kernel module
	ret = sprintf(msg, "%d", STOP_CODE);
	if (ret < 0)
	{
		printf ("errore in sprintf()\n");
	}
	strcat(msg, "\0");
	send_kernel_message(msg);
	receiveOKCode();


	//send the target address to the filter
	//messaggio => CODE-IP
	ret = sprintf(msg, "%d", TARGET_IP_CODE);
	if (ret < 0)
	{
		printf ("errore in sprintf()\n");
		std::raise(SIGINT);
	}
	strcat(msg, "-");
	strcat(msg, IP);
	strcat(msg, "\0");
	//printf("msg %s\n", msg);
	send_kernel_message(msg);
	//blocking receive
	receiveOKCode();


	//send TTL_MAX to the filter
	//messaggio => CODE-TTL_max
	ret = sprintf(msg, "%d", MAX_TTL_CODE);
	if (ret < 0)
	{
		printf ("errore in sprintf()\n");
		std::raise(SIGINT);
	}
	strcat(msg, "-");
	sprintf(s, "%d", maxDepth);
	strcat(msg, s);
	strcat(msg, "\0");
	//printf("msg %s\n", msg);
	send_kernel_message(msg);
	//blocking receive
	receiveOKCode();


	//send TTL_MIN to the filter
	//messaggio => CODE-TTL_min
	ret = sprintf(msg, "%d", MIN_TTL_CODE);
	if (ret < 0)
	{
		printf ("errore in sprintf()\n");
		std::raise(SIGINT);
	}
	strcat(msg, "-");
	sprintf(s, "%d", lastTCPHop);
	strcat(msg, s);
	strcat(msg, "\0");
	//printf("msg %s\n", msg);
	send_kernel_message(msg);
	//blocking receive
	receiveOKCode();


	//send MAX_ATTEMPT to the filter
	//messaggio => CODE-MAX_ATTEMPT
	ret = sprintf(msg, "%d", MAX_ATTEMPT_CODE);
	if (ret < 0)
	{
		printf ("errore in sprintf()\n");
		std::raise(SIGINT);
	}
	strcat(msg, "-");
	sprintf(s, "%d", MAX_ATTEMPT);
	strcat(msg, s);
	strcat(msg, "\0");
	//printf("msg %s\n", msg);
	send_kernel_message(msg);
	//blocking receive
	receiveOKCode();
}



void receiveOKCode()
{
	char* buffer;
	buffer = (char *)malloc(sizeof(char) * 10);
	memset(buffer, '\0', sizeof(char) * 10);

	receive_kernel_message(0, buffer);
	int recvCode = atoi(buffer);

    if (recvCode != OK_CODE)
	{
		printf("Received message payload: %s\n", buffer);
		free(buffer);
		std::raise(SIGINT);
	}
    //printf("Received message payload: %s\n", buffer);	

	free(buffer);
}



void receiveStartCode()
{
	char * buffer = (char*)malloc(sizeof(char) * 10);
	memset(buffer, '\0', sizeof(char) * 10);

	receive_kernel_message(0, buffer);
	int recvCode = atoi(buffer);

    if (recvCode != START_CODE)
	{
		printf("Received message payload: %s\n", buffer);
		free(buffer);
		std::raise(SIGINT);
	}
    printf("Received the starting message from the kernel \n");	

	free(buffer);
}