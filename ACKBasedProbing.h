#define RECONNECT_DISABLED -1
#define RECONNECT_ENABLED 1

#define KEEP_ALIVE_DISABLED 0
#define KEEP_ALIVE_ENABLED 1

#define HOP_TRACKING_DISABLED 0
#define HOP_TRACKING_ENABLED 1

#define MAX_ATTEMPT 3
#define MAX_TTL 64

#define NETLINK_USER 31
#define MAX_NETLINK_PAYLOAD 1024 /* maximum payload size*/




#include <stddef.h>
#include <stdint.h>
#include <signal.h>
#include <map>
#include <netinet/in.h>
#include <sys/time.h>


int  maxDepth = 0,
     lastTCPHop = -1;

int sd, 
    raw_sock,
    netlink_sd;

uint8_t waitTime;
uint16_t source_port = 0;

static volatile int keepAliveOption = KEEP_ALIVE_DISABLED,
		            reconnectOption = RECONNECT_DISABLED,
		            hopTracking = HOP_TRACKING_DISABLED,
		            inc = 0,
					lastTTL = 0,
					lastAttemptNumber = 0;



volatile uint8_t actualTTL = 1;

static volatile sig_atomic_t sigpipeOccurred = 0;

char serverAddr[150],
     serverName[500],
     request[1024];

struct sockaddr_in sv_addr,
				   my_addr;

struct traceroute
{
	int hop;
	int attempt;
	char IPRouter[20];
	double tempo;
	struct traceroute* next;
};
struct traceroute* head=NULL;


//tuple[0] = rcvTTL
//tuple[1] = rcvAttemptNumber
//tuple[2] = startTime (when the kernel sent the ttl)
std::map<std::pair<uint32_t, uint16_t>, std::tuple<int,int, double>> ttlSeqMap;
std::map<std::pair<uint32_t, uint16_t>, std::pair<char*, double>>pendingRcvdICMPPckt;



void clearQueue(void);
void printQueue(void);
void insertQueue(int, char*, double, int);

void atExit(void);
void handlerQUIT(int);
void handlerPipe(int);

int openingConnection(void);

int processPacket(char*, int, int, double);
int performPathInspection(void);

void receiveICMPPackets(void);

void handleParameters(int, char*);

int open_kernel_socket();
int send_kernel_message(char*);
int receive_kernel_message(int, char*);

void sendInitialParameter(char*);
void receiveOKCode();
void receiveStartCode();


