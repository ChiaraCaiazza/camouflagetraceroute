#include "DATABasedProbing.h"



void clearQueue(){
	struct traceroute* element = head;

	while(element != NULL){
		head = head -> next;
		free(element);
		element = head;
	}
}



void printQueue(){
	struct traceroute* element = head;

	while(element != NULL){
	    if (element -> tempo >= 0)
		{
	        //tempo >= 0 -> hop found
	        if (hopTracking == HOP_TRACKING_ENABLED)
	            printf("%d) %s  [%d]   Tempo: %f ms\n", element->hop, element->IPRouter, element->attempt, element->tempo);
	        else
	            printf("%d) %s     Tempo: %f ms\n", element->hop, element->IPRouter, element->tempo);
		}
	    else
		{
	        //tempo < 0 -> hop not found
	        if (hopTracking == HOP_TRACKING_ENABLED)
	            //HOP_TRACKING_ATTEMPT -> print the attempt #
                printf("%d) %s   [%d]\n", element->hop, element->IPRouter, element->attempt);
            else
                printf("%d) %s\n", element->hop, element->IPRouter);
		}

		element = element -> next;
	}
}



void insertQueue(int hopNum, char* address, double timestamp, int attempt){
    if(head == NULL){
        //add the first element
        head = (struct traceroute*)malloc(sizeof(struct traceroute));
        last = head;
    }
    else{
        last->next = (struct traceroute*)malloc(sizeof(struct traceroute));
        last = last->next;
    }

    if(last == NULL){
        printf("Error: unallocated memory!\n");
        exit(0);
    }

	memset(&last->IPRouter, '\0' , sizeof(last->IPRouter));
    strncpy(last->IPRouter, address, strlen(address));
    last->hop = hopNum;
    last->attempt = attempt;
    last->tempo = timestamp;
    last->next = NULL;
}



void handlerQUIT(int sig){
	printf("\n\nSocket closure....\n");

	close(sd);
	close(raw_sock);

    printQueue();
	clearQueue();

	exit(0);
}



void handlerPipe(int sig){
    int ret;

    //ignoring new sigpipe in handlerPipe
	signal(SIGPIPE, SIG_IGN);

	inc = 0;
	sigpipeOccurred = 1;

    printf("\n\nSIGPIPE detected\n\n");
    if (verboseMode)
		fprintf(stderr, "\n\nSIGPIPE detected\n\n");

    //close the socket
	close(sd);

	if (reconnectOption == RECONNECT_DISABLED)
	{
	    //now -c option is not used. Clean the environment before opening a new connection.
        clearQueue();
	    actualTTL = 0;

        //options upgrading
		if (keepAliveOption == KEEP_ALIVE_DISABLED)
		{
			printf("\n- - - - - - - - - - - - - - - - - \nBroken stream, retry with -k option\n");
			keepAliveOption = KEEP_ALIVE_ENABLED;
		}
		else if (keepAliveOption == KEEP_ALIVE_ENABLED)
		{
			printf("\n- - - - - - - - - - - - - - - - - \nBroken stream, retry with -k -c options\n");
			reconnectOption = RECONNECT_ENABLED;
		}
	}
	else
	    //now -c option is used. The new connection starts from the last TTL
		--actualTTL;

    //establish a new connection
    ret = openingConnection();
    if (ret < 0)
        exit(0);

    //enabling again sigpipe catching
	signal(SIGPIPE, handlerPipe);

	return;
}



int openingConnection(){
    int ret,
		residualBindingTime = 5*60; //we try to bind a source port for at most 5 minutes
    struct timeval sdTimer;

    //open the socket (TCP)
    sd = socket(AF_INET, SOCK_STREAM , 0);
	if(sd < 0){
		printf("Error in socket creation\n");
		if (verboseMode)
			fprintf(stderr, "Error in socket creation\n");

		return -1;
	}

	if (source_port != 0)
	{
		memset(&my_addr, 0, sizeof(my_addr));
		my_addr.sin_family = AF_INET;
		my_addr.sin_addr.s_addr = htonl(INADDR_ANY);
		my_addr.sin_port = htons(source_port);

		while(1)
		{
			ret = bind(sd, (struct sockaddr*) &my_addr, sizeof(my_addr));
			if (ret < 0)
			{
				if (errno == EADDRINUSE)
				{
					residualBindingTime -= 5;
					if (residualBindingTime < 0)
					{
						printf("Too many bind attempts... EXITING\n");
						fprintf(stderr, "Too many bind attempts... EXITING\n");

						exit(0);
					}
					sleep(10);
					
					continue;
				}

				printf("Error in client bind()\n");
				printf("(%d)%s\n", errno, strerror(errno));
				if (verboseMode)
				{
					fprintf(stderr, "Error in client bind()\n");
					fprintf(stderr, "(%d)%s\n", errno, strerror(errno));
				}

				exit(0);
			}
			else 
				break;
		}
	}

	//connect with the server
    ret = connect(sd, (struct sockaddr*)&sv_addr, sizeof(sv_addr));
    if ((ret) == -1)
    {
        printf("Failed to connect to the server: %s\n", strerror(errno));
        if (verboseMode)
			fprintf(stderr, "Failed to connect to the server: %s\n", strerror(errno));

        return -1;
    }

    //imposto timer (2 sec.) per la recvfrom
	sdTimer.tv_sec = 2;
	sdTimer.tv_usec = 0;
    if(setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO,(const char*)&sdTimer, sizeof(struct timeval))<0){
		printf("Error in the association of the timer\n");
		if (verboseMode)
			fprintf(stderr, "Error in the association of the timer\n");
		return -1;
	}


	if (source_port == 0)
	{
		//per ottetenere la struttura sockaddr_in contenente i miei valori
		socklen_t my_addrlen = sizeof(my_addr);
		//getsockname() returns the current address to which the socket sd is bound
		if(getsockname(sd, (struct sockaddr*)&my_addr, &my_addrlen)==-1){
			printf("Getsockname error\n");
			return -1;
		}
		myPort = ntohs(my_addr.sin_port);
	}
	else
		myPort = source_port;

	return 1;
}



int processPacket(char* buff, int dim, int hopNum, double temp, int attempt){
	unsigned short iphdlen,
		           iperrlen;
	struct ip* IPerr;
	struct ip* iphd = (struct ip*)buff;
	struct tcphdr* tcphd;


	//pointer to the ICMP header
	iphdlen = iphd->ip_hl*4;
	struct icmphdr* icmp = (struct icmphdr*)(buff + iphdlen);


	if(icmp->type == 11 && icmp->code == 0){
		//ICMP type == TIME_EXCEEDED

		//8 bytes = ICMP header length (4 bytes) + unused field (4 byte)
		//IPerr is the pointer to the Internet Header + 64 bits of
		//	Original Data Datagram
		IPerr = (struct ip*)(buff + iphdlen + 8);

        //check the dest address in the original pkt
		if(strcmp(inet_ntoa(IPerr->ip_dst), inet_ntoa(sv_addr.sin_addr)) == 0){

			iperrlen = IPerr->ip_hl*4;
			tcphd = (struct tcphdr*)(buff + iphdlen + 8 + iperrlen);

			//check the port number in the header of the original pkt
			if(ntohs(tcphd->th_sport) == myPort){
			    //ICMP pkt received. Add a new element in the queue
			    insertQueue(hopNum, inet_ntoa(iphd->ip_src), temp, attempt);

				return 1;
			}
		}
	}

    //0 -> not found
	return 0;
}



int  parseRecvMessage(char* recvMessage){
    char substr[15];
    char* startPosition = recvMessage;
    int contentLength = -1,
        payloadLength = -1;


    printf("Original message %s \n", recvMessage);
	printf("Length of the original message: %d\n", (int)strlen(recvMessage));


	while (1)
	{
	    memset(substr, '\0', sizeof(substr));

	    strncpy(substr, startPosition, sizeof("Content-Length:"));
	    if (strstr(substr, "Content-Length:") != NULL)
	    {

	        memset(substr, '\0', sizeof(substr));

	        startPosition = startPosition + sizeof("Content-Length:");
	        strncpy(substr, startPosition,  4);
	        startPosition = startPosition + 4;

	        contentLength = atoi(substr);

	    }

	    if (strstr(substr, "<") != NULL && strstr(substr, "\n") == NULL && payloadLength == -1 )
            payloadLength = (int)strlen(startPosition);


	    startPosition = strstr(startPosition, "\n");


	    if (startPosition == NULL)
	        break;


        startPosition = startPosition + 1;
	}


    printf("\n\nCONTENTLENGTH: %d\n", contentLength);
    printf ("Payload Length = %d\n", payloadLength);


    return 1;
}



int performPathInspection(){
	uint8_t maxTTL = MAX_TTL;
	int inc = 0,
	    ret,
	    byte = 1,
	    attempt = 0;
	double before,
	       after;
	struct timeval timems;
	char receivedPacket[1024];



	for(; actualTTL <= maxDepth; actualTTL++){
	    int found = 0;

	    //change the TTL to actual TTL
		if(setsockopt(sd, IPPROTO_IP, IP_TTL, (uint8_t*) &actualTTL, sizeof(actualTTL)) < 0){
			printf("Setsockopt error\n");
			return -1;
		}

        //send the request
		if(keepAliveOption == KEEP_ALIVE_ENABLED)
		    //option -k is setted: for each pkt send 1 byte
			ret = send(sd, &request[(++inc)], byte, 0);
		else
		    //option -k is setted: for each pkt send the whole request
			ret = send(sd, request, strlen(request), 0);
		
		if (verboseMode)
			fprintf(stderr, "\n\n\n(TTL %d, attempt #%d) send %d bytes\n", actualTTL, attempt, ret);

		if (sigpipeOccurred)
		{
		    printf("SIGPIPE!\n");
			sigpipeOccurred = 0;
			continue;
		}


        //prelevo il tempo da assegnare a before (number of milliseconds)
		gettimeofday(&timems, NULL);
		before = (double)timems.tv_sec*1000 + (double)timems.tv_usec/1000;

		//change the TTL to its maximum value
		if(setsockopt(sd, IPPROTO_IP, IP_TTL, &maxTTL, sizeof(maxTTL)) < 0){
			printf("Setsockopt error\n");
			fprintf(stderr, "Setsockopt error\n");
			return -1;
		}


        //wait for an ICMP-TTL expired packet
		while(1){
		    if (verboseMode)
				fprintf(stderr, "\twait for an ICMP-TTL expired packet\n");

            //receive a pkt
            memset(&receivedPacket, '\0' , sizeof(receivedPacket));
            ret=recvfrom(raw_sock, receivedPacket, 1024, MSG_WAITALL, NULL, NULL);
			if(ret == -1)
			{
				break;
			}

			//an ICMP pkt is receved
			gettimeofday(&timems,NULL);
			after=(double)timems.tv_sec*1000+(double)timems.tv_usec/1000;

			found = processPacket(receivedPacket, ret, actualTTL, after-before, attempt + 1);
			if (found)
			{
			    if (verboseMode)
					fprintf(stderr, "\t\tICMP message received\n");
			    attempt = 0;
			    break;
			}

            //we stay in the cycle for waitTime seconds (waitTime*1000 milliseconds)
			gettimeofday(&timems, NULL);
			if(((double)timems.tv_sec*1000+(double)timems.tv_usec/1000)-before>(waitTime*1000))
				break;
		}

        //wait for a response from the TCP connection
        while(1)
        {
            if (verboseMode)
				fprintf(stderr, "\twait for a response from the TCP connection\n");
            errno = 0;

            //wait for a packet to be received
            memset(&receivedPacket,0,sizeof(receivedPacket));
            ret=recvfrom(sd,receivedPacket,1024,MSG_WAITALL,NULL, NULL);
            if (verboseMode)
			{
				fprintf (stderr, "\t\tTTL = %d) number of bytes returned = %d, errno = %s\n", actualTTL, ret, strerror(errno));
            	fprintf (stderr, "%s\n", receivedPacket);
			}

            if (ret == 0)
            {
                //the server closed this connection
                if (verboseMode)
					fprintf(stderr, "\n!!! closing connnection at hop %d... \n\n\n", actualTTL);
			    close(sd);

                if (verboseMode)
					fprintf(stderr, "opening a new connection... \n");
                ret = openingConnection();
                if (ret < 0)
                    exit(0);
                break;
			}

			if (ret < 0)
			{
			    if (verboseMode)
					fprintf(stderr, "\t\tRET < 0\n");
			    break;
			}

		}



        if(!found && attempt < (MAX_ATTEMPT -1))
        {
            if (verboseMode)
				fprintf(stderr, "FAIL:\tHOP %d (attempt %d) not found. Try again\n", actualTTL, attempt);

            attempt++;
           --actualTTL;
        }
        else if (!found)
        {
            if (HOP_TRACKING_ENABLED)
                insertQueue(actualTTL, "*", -1, attempt + 1);
            else
                insertQueue(actualTTL, "*", -1, -1);

            attempt = 0;
        }
    }

    return 1;
}



void handleParameters(int argc, char*argv[]){
    int ret;
    // getopt non stampa mess errore
	opterr = 0;

	while((ret = getopt(argc, argv, "kcwdantvhs")) !=- 1)
		switch(ret){
			case 'v':
				verboseMode = VERBOSE_MODE_ENABLED;
				printf("verbose\n");
				
				if (verboseMode)
					fprintf(stderr, "VERBOSE\n");
				
				break;
				
			case 'k':
				keepAliveOption = KEEP_ALIVE_ENABLED;
				printf("KEEPALIVE OPTION ENABLED\n");

			    if (verboseMode)
					fprintf(stderr, "KEEPALIVE OPTION ENABLED\n");

				break;

			case 'c':
				reconnectOption = RECONNECT_ENABLED;
				printf("RECONNECT OPTION ENABLED\n");

				if (verboseMode)
					fprintf(stderr, "RECONNECT OPTION ENABLED\n");
				
				break;

			case 't':
				hopTracking = HOP_TRACKING_ENABLED;
				printf("HOP TRACKING OPTION ENABLED\n");

				if (verboseMode)
					fprintf(stderr, "HOP TRACKING OPTION ENABLED\n");
				
				break;

			case 'w':
				waitTime = atoi(argv[optind]);
				if (waitTime > 10 || waitTime < 1)
				{
					printf("Error: The -w value has to be set in the [1, 10] range (in seconds)\n");

					if (verboseMode)
						fprintf(stderr, "Error: The -w value has to be set in the [1, 10] range (in seconds)\n");
					exit(0);
				}

				printf("WAIT TIME = %i\n", waitTime);

				if (verboseMode)
					fprintf(stderr, "WAIT TIME = %i\n", waitTime);

				break;

			case 'a':
				strcpy(serverAddr, argv[optind]);
				printf("TARGET ADDRESS = %s\n", serverAddr);

				if (verboseMode)
					fprintf(stderr, "TARGET ADDRESS = %s\n", serverAddr);

				break;

			case 'd':
				strcpy(serverName, argv[optind]);
				printf("TARGET = %s\n", serverName);
				
				if (verboseMode)
					fprintf(stderr, "TARGET = %s\n", serverName);

				break;

			case 'n':
			    maxDepth = atoi(argv[optind]);
				printf("MAX_DEPTH = %d\n",  maxDepth);

				if (verboseMode)
					fprintf(stderr, "MAX_DEPTH = %d\n",  maxDepth);

				break;

			case 'h':
				printf("Usage:\nsudo ./Trace.o [ -c ] [-k] [ -w wait_time ] [-s source_port] ");
				printf("[ -a IPv4_target_address ] [ -d target_domain_name ] [ -n max_hop_number] [ -t]");
				printf(" [-v] \n");

				printf("Options:\n");
				printf("-c\t\t\t\tUse the reconnect option\n");
				printf("-k\t\t\t\tSend HTTP request of 1 byte length\n");
				printf("-w wait_time\t\t\tWait for an ICMP packet no more than wait_time seconds.\n");
				printf("\t\t\t\tThe wait_time has to be set in the [1-10] interval.\n");
				printf("-s source_port\t\t\tUse source_port value for outgoing packets.\n");
				printf("\t\t\t\tThe source_port has to be set in the [40000 - 60000] interval.\n");
				printf("-a IPv4_target_address\t\tSet the target IPv4 address\n");
				printf("-d target_domain_name\t\tSet the target domain name\n");
				printf("-n max_hop_number\t\tSet the max TTL to be reached.\n");
				printf("-t\t\t\t\tRecord the attempt number\n");
				printf("-v\t\t\t\tVerbose\n ");
						
				exit(0);
				break;

			case 's':
				source_port = (uint16_t)atoi(argv[optind]);
				if (source_port > 60000 || source_port < 40000)
				{
					printf("Error: The -s value has to be set in the [40000 - 60000] range\n");

					if (verboseMode)
						fprintf(stderr, "Error: The -s value has to be set in the [40000 - 60000] range\n");
					exit(0);
				}
				printf("SOURCE_PORT = %d\n",  source_port);

				if (verboseMode)
					fprintf(stderr, "SOURCE_PORT= %d\n",  source_port);

				break;
			
			case '?':
				// Checks whether optopt is a printable character.
				
				if(isprint(optopt))
					printf("Unknown option '-%c'.\n", optopt);
				else
					printf("Unknown option character '\\x%x'.\n", optopt);
				exit(0);
			default:
				exit(0);
		}



	printf("MAX_ATTEMPT = %d\n", MAX_ATTEMPT);
	if (verboseMode)		
		fprintf(stderr, "MAX_ATTEMPT = %d\n", MAX_ATTEMPT);
}



/******************************************************************************
	prende un nome simbolico, interroga il dns. ritorna un oggetto
        hostent con l'indirizzo IP o NULL se la ricerca non fornisce alcun
        risultata
******************************************************************************/
char* getIP(char hostName[40]){
	struct hostent* ServerInfo;

	ServerInfo = gethostbyname(hostName);
	if(ServerInfo == NULL){
		printf("No server found with '%s' as hostname\n", hostName);
		exit(0);
	}

	return inet_ntoa(*(struct in_addr*)ServerInfo->h_addr);
}



int main(int argc, char*argv[]){
    int ret;
    char IP[40],
         bufferHTTP[2056];
	struct timeval timer;

	fprintf(stderr, "camotrace v1.2\n");

	signal(SIGINT, handlerQUIT);
	signal(SIGPIPE, handlerPipe);


	memset(&sv_addr, '\0', sizeof(sv_addr));
	memset(&serverAddr, '\0', sizeof(serverAddr));
	memset(&serverName, '\0', sizeof(serverName));
	memset(&bufferHTTP, '\0', sizeof(bufferHTTP));
	memset(&request, '\0', sizeof(request));


	handleParameters(argc, argv);


    //create the raw socket
	raw_sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
	if(raw_sock < 0){
		printf("Error in raw socket creation\n");
		if (verboseMode)
			fprintf(stderr, "Error in raw socket creation\n");

		return -1;
	}

	//imposto timer (2 sec.) per la recvfrom
	timer.tv_sec = waitTime;
	timer.tv_usec = 0;
	if(setsockopt(raw_sock, SOL_SOCKET, SO_RCVTIMEO,(const char*)&timer, sizeof(struct timeval))<0){
		printf("Error in the association of the timer\n");
		if (verboseMode)
	    	fprintf(stderr, "Error in the association of the timer\n");

		return -1;
	}


	strncpy(IP, getIP(serverAddr), 40);
	sv_addr.sin_family = AF_INET;
	sv_addr.sin_port = htons(80);

	//This function converts the character string src into a network
    //address structure in the selected address family
	inet_pton(AF_INET, IP, &sv_addr.sin_addr);

    sprintf(request, "GET / HTTP/1.1\r\nHost: %s\r\nConnection: keep-alive\r\n\r\n", serverName);


    // connessione al server
	printf("\nConnecting to %s (%s) .....\n", serverAddr, IP);
	if (verboseMode)
		fprintf(stderr, "\nConnecting to %s (%s) .....\n", serverAddr, IP);
    ret = openingConnection();

    if (ret >0)
    {
        printf("Connected\n\n");
        if (verboseMode)
			fprintf(stderr, "Connected\n\n");

        printf("REQUEST:\n%s\n\n", request);
        if (verboseMode)
			fprintf(stderr, "REQUEST:\n%s\n\n", request);

        ret = performPathInspection();
        if (ret < 0)
        {
            printf("Error in performPathInspection()\nExiting\n");
            if (verboseMode)
				fprintf(stderr, "Error in performPathInspection()\nExiting\n");

            return -1;
        }
    }
    else
        return -1;

    handlerQUIT(0);

	return ret;
}
