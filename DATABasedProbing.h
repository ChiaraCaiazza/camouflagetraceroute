#define RECONNECT_DISABLED -1
#define RECONNECT_ENABLED 1

#define KEEP_ALIVE_DISABLED 0
#define KEEP_ALIVE_ENABLED 1

#define HOP_TRACKING_DISABLED 0
#define HOP_TRACKING_ENABLED 1

#define VERBOSE_MODE_DISABLED 0
#define VERBOSE_MODE_ENABLED 1

#define MAX_ATTEMPT 3
#define MAX_TTL 64


#include <stddef.h>
#include <stdint.h>
#include <signal.h>
#include<arpa/inet.h>
#include<netinet/ip_icmp.h>
#include<netinet/tcp.h>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<netdb.h>
#include<sys/time.h>
#include<errno.h>
#include<unistd.h>
#include<ctype.h>
#include<fcntl.h>


int  maxDepth = 0;

int sd, 
    raw_sock,
    padre;

uint8_t waitTime;
uint16_t myPort;

static volatile int keepAliveOption = KEEP_ALIVE_DISABLED,
		            reconnectOption = RECONNECT_DISABLED,
		            hopTracking = HOP_TRACKING_DISABLED,
					verboseMode = VERBOSE_MODE_DISABLED,
					inc = 0;

volatile uint8_t actualTTL = 1;
uint16_t source_port = 0;

static volatile sig_atomic_t sigpipeOccurred = 0;

char serverAddr[150],
     serverName[500],
     request[1024];

struct sockaddr_in sv_addr,
       			   my_addr;


struct traceroute
{
	int hop;
	int attempt;
	char IPRouter[20];
	double tempo;
	struct traceroute* next;
};
struct traceroute* head=NULL;
struct traceroute* last=NULL;



void handlerPipe(int);
int openingConnection();