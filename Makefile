obj-m := camoFilter.o

all:  
	make ACKBasedProbing 
	make DATABasedProbing
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD)/netfilter modules


ACKBasedProbing : ACKBasedProbing.cpp
	g++ ACKBasedProbing.cpp ACKBasedProbing.h --std=c++11 -Wall -o ACKBasedProbing

DATABasedProbing : DATABasedProbing.c
	gcc DATABasedProbing.c DATABasedProbing.h -Wall -o DATABasedProbing

.PHONY: clean
clean:
	rm -rf ACKBasedProbing DATABasedProbing
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD)/netfilter clean


