# Camouflage traceroute



## Installation
Clone the repository and use `make` to compile the code.

## DATA-based Usage
	sudo ./DATABasedProbing [-c] [-k] -w wait_time [-s src_port] -a IPv4_target_address -d target_domain_name -n max_hop_number [-t] [-h] 

Options	  		 				|  
-------------------------------:|:-----
**-c**    						|Use camotrace with reconnect option enabled (default id disabled).
**-k**	  						|Send HTTP requests of 1 byte length (default is disabled).
**-w** 	*wait_time*				|Wait for an ICMP packet no more than *wait_time* seconds. *wait_time* value has to be set in the [1-10] interval.l.
**-s** *src_port*				|Establish a TCP connection using *src_port* as source_port. *src_port* has to be set in the [40000 - 60000] interval.
**-a** *IPv4_target_address*	|Set the target IPv4 address.
**-d** *target_domain_name*		|Set the target domain name.
**-n** *max_hop_number*			|Set the max TTL to be reached.
**-t**							|Add the attempt number to the result. Default: disabled
**-h**							|help


## ACK-based Usage

	cd netfilter
	sudo insmod camoFilter.ko
	cd ..
	
	sudo ./ACKBasedProbing [-c] [-k] -w wait_time [-s src_port] -a IPv4_target_address -d target_domain_name -n max_hop_number [-t] [-h] [-l minimum_hop_number]

Options	  		 				|  
-------------------------------:|:-----
**-c**    						|Use camotrace with reconnect option enabled.
**-k**	  						|Send HTTP requests of 1-byte length.
**-w** 	*wait_time*				|Wait for an ICMP packet no more than *wait_time* seconds. *wait_time* value has to be set in the [1-10] interval.l.
**-s** *src_port*				|Establish a TCP connection using *src_port* as source_port. *src_port* has to be set in the [40000 - 60000] interval.
**-a** *IPv4_target_address*	|Set the target IPv4 address.
**-d** *target_domain_name*		|Set the target domain name.
**-n** *max_hop_number*			|Set the max TTL to be reached.
**-t**							|Add the attempt number to the result
**-l** *minimum_hop_number*		|The first hop number probed.
**-h**							|help

				
		
			
				

