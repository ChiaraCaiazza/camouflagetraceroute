#include "camoFilter.h"
#include "messageCodes.h"


#include <net/icmp.h>




int charToInt(char *s)
{
    int n = 0,
        i = 0;

    while (s[i] != '\0')
    {
        n = n * 10 + (int)s[i] - '0';
        i++;        
    }

    return n;
}



static void hello_nl_recv_msg(struct sk_buff *skb)
{
    struct nlmsghdr *nlh;
    char *receivedMsg;
        // *strstrPointer;
    char returnCode[10],
         code_c[3],
         str[3];

    int code = 0;
         
 
    nlh = (struct nlmsghdr *)skb->data;
    /*pid of sending process */
    pid = nlh->nlmsg_pid;

    receivedMsg = (char *)nlmsg_data(nlh);       
    code_c[0] = receivedMsg[0];
    code_c[1] = receivedMsg[1];
    code_c[2] = '\0';

    code = charToInt(code_c);
    
    switch (code)
    {
        case TARGET_IP_CODE:
            memset(targetIP, '\0', sizeof(char)*strlen(targetIP));
            strcpy(targetIP, receivedMsg + 3);
            printk(KERN_INFO "Received target address:\t %s\n", targetIP);

            sprintf(returnCode, "%d\n", OK_CODE);
            send_kernel_message(returnCode);

            if (stat == INITIALIZING)
                handleInitializingState();

            break;

        case MAX_TTL_CODE:
            memset(str, '\0', sizeof(str));
            strcpy(str, receivedMsg + 3);
            ttl_max = charToInt(str);
            printk(KERN_INFO "Received ttl_max = %d\n", ttl_max);

            sprintf(returnCode, "%d\n", OK_CODE);
            send_kernel_message(returnCode);

            if (stat == INITIALIZING)
                handleInitializingState();

            break;

        case MIN_TTL_CODE:
            memset(str, '\0', sizeof(str));
            strcpy(str, receivedMsg + 3);
            ttl_min = charToInt(str);
            printk(KERN_INFO "Received ttl_min = %d\n", ttl_min);

            sprintf(returnCode, "%d\n", OK_CODE);
            send_kernel_message(returnCode);

            if (stat == INITIALIZING)
                handleInitializingState();

            break;

        case MAX_ATTEMPT_CODE:
            memset(&str, '\0', sizeof(str));
            strcpy(str, receivedMsg + 3);
            N_attempt_max = charToInt(str);
            printk(KERN_INFO "Received N_attempt_max = %d\n", N_attempt_max);     
            
            sprintf(returnCode, "%d\n", OK_CODE);
            send_kernel_message(returnCode);  

            if (stat == INITIALIZING)
                handleInitializingState(); 

            break;

        case STOP_CODE:
            memset (targetIP, '\0', sizeof(char) * 15);
            ttl_max = -1;
            ttl_min = -1;
            N_attempt_max = -1;
            stat = INITIALIZING;

            printk(KERN_INFO "STOP received\nStat = %d\n", stat);

            sprintf(returnCode, "%d\n", OK_CODE);
            send_kernel_message(returnCode);

            break;

        default:
            break;
        

    }

    
}



void send_kernel_message(char* msg)
{
    int msg_size,
        res;
    struct sk_buff *skb_out;
    struct nlmsghdr *nlh;
    //int pid;

    msg_size = strlen(msg);

    skb_out = nlmsg_new(msg_size, 0);
    if (!skb_out)
    {
        printk(KERN_ERR "Failed to allocate new skb\n");
        return;
    }

    nlh = (struct nlmsghdr *)skb_out->data;
    nlh = nlmsg_put(skb_out, 0, 0, NLMSG_DONE, msg_size, 0);
    /* not in mcast group */
    NETLINK_CB(skb_out).dst_group = 0;
    strncpy(nlmsg_data(nlh), msg, msg_size);

    res = 0;
    //while (res < 0)
    {
        res = nlmsg_unicast(nl_sk, skb_out, pid);
        if (res < 0)
            printk(KERN_INFO "Error while sending back to user\n");
    }

    return;

}



//function to be called by hook
unsigned int inputHook_func(void *priv,
                   struct sk_buff *skb,
                   const struct nf_hook_state *state)
{	
    //uint16_t source_port, 
    //         dest_port;
    char rcv_dst[16],
         rcv_src[16];

    if (targetIP == NULL || strlen(targetIP) == 0)
        return NF_ACCEPT;
        
    
    sock_buff = skb;
    //grab the network header
    ip_header = (struct iphdr* ) skb_header_pointer(sock_buff, 0, 0, NULL);
    if (!sock_buff)
    {
        printk(KERN_INFO "!sock_buff\n"); 
        return NF_ACCEPT;
    }
         
    //we sniff only for tcp & ICMP packets
    if (ip_header->protocol != IPPROTO_TCP && ip_header->protocol != IPPROTO_ICMP)
        return NF_ACCEPT;
    
    
    //IPv4 addresses
    snprintf(rcv_src, 16, "%pI4", &ip_header->saddr);
    snprintf(rcv_dst, 16, "%pI4", &ip_header->daddr);
    if (ip_header->protocol == IPPROTO_TCP && strncmp (rcv_src, targetIP, 15) != 0)
        //the receiver is not the target host
        return NF_ACCEPT; 
    
      
    if (ip_header->protocol == IPPROTO_TCP)
    {
        char *payload;
        struct tcphdr *tcp_header =  (struct tcphdr *)tcp_hdr(skb);


        printk(KERN_INFO "\n(input)STATO:%d\n",stat);
        printk(KERN_INFO "protocol: TCP\n");
        printk(KERN_INFO "source addr:%s\n", rcv_src);
        printk(KERN_INFO "destination addr:%s\n", rcv_dst);


        payload = (char *)tcp_header + tcp_hdrlen(skb);

            if (strlen(payload) > 0 && strstr(payload, "HTTP") != NULL)
            {
                printk(KERN_INFO "payload -> %s\n", payload);
                if(targetTTL > ttl_min)
                    replyRcvd++;
                    
                printk(KERN_INFO "replyRcv: %d\n", replyRcvd);

                if (replyRcvd > 5)
                {
                    char msg[2];
                    printk(KERN_INFO "stop\n");

                    memset (targetIP, '\0', sizeof(char) * 15);
                    ttl_max = -1;
                    ttl_min = -1;
                    targetTTL = -1;
                    N_attempt_max = -1;
                    actualNAttempt = -1;
                    stat = INITIALIZING;
                    replyRcvd = 0;

                    sprintf(msg, "%d", STOP_CODE);
                    send_kernel_message(msg);
                    return NF_ACCEPT;
                }

            }



    }
     if (ip_header->protocol == IPPROTO_ICMP)
    {
        struct icmphdr* icmp_header = (struct icmphdr *)icmp_hdr(skb);
      
        if (icmp_header->type == 11 && icmp_header->code == 0)
        {
            struct iphdr *originalIPv4Hdr;
            char originalDstAddr[16];

            printk(KERN_INFO "\n(input)STATO:%d\n",stat);
            printk(KERN_INFO "protocol: ICMP (TTL exceedeed)\n");
            printk(KERN_INFO "source addr:%s\n", rcv_src);
            printk(KERN_INFO "destination addr:%s\n", rcv_dst);
            printk(KERN_INFO "checksum: %d\n", icmp_header->checksum);
            printk(KERN_INFO "type: %d\n", icmp_header->type);
            printk(KERN_INFO "code: %d\n", icmp_header->code);

            originalIPv4Hdr = (struct iphdr*)(icmp_header + 1);
            snprintf(originalDstAddr, 16, "%pI4", &originalIPv4Hdr->daddr);
            printk(KERN_INFO "external dest addr: %s\n", originalDstAddr);

            if (strncmp (originalDstAddr, targetIP, 15) == 0)
                replyRcvd = 0;

            printk(KERN_INFO "replyRcv: %d\n", replyRcvd);                        
        }
	

    }

    return NF_ACCEPT;            
}


//function to be called by hook
unsigned int outputHook_func(void *priv,
                   struct sk_buff *skb,
                   const struct nf_hook_state *state)
{	
    uint16_t source_port, 
             dest_port;
    char rcv_dst[16],
         rcv_src[16];

    if (targetIP == NULL || strlen(targetIP) == 0)
        return NF_ACCEPT;
        
    
    sock_buff = skb;
    //grab the network header
    ip_header = (struct iphdr* ) skb_header_pointer(sock_buff, 0, 0, NULL);
    if (!sock_buff)
    {
        printk(KERN_INFO "!sock_buff\n"); 
        return NF_ACCEPT;
    }
         

    //we modify only tcp packets
    if (ip_header->protocol != IPPROTO_TCP )
        return NF_ACCEPT;
    //this packet is a tcp packet
    
    
    //IPv4 addresses
    snprintf(rcv_src, 16, "%pI4", &ip_header->saddr);
    snprintf(rcv_dst, 16, "%pI4", &ip_header->daddr);
    if (strncmp (rcv_dst, targetIP, 15) != 0)
        //the receiver is not the target host
        return NF_ACCEPT;    
    //this is a tcp packet with the target host as receiver
    
    
    //pointer to the tcp header
	tcp_header =  (struct tcphdr *)tcp_hdr(skb);
    
    //port numbers
    dest_port = (uint16_t) ntohs (tcp_header->dest);
    source_port = (uint16_t) ntohs (tcp_header->source);
    
    
    printk(KERN_INFO "\nSTATO:%d\n",stat);    
    printk(KERN_INFO "source port:%d\n", source_port);
    printk(KERN_INFO "source addr:%s\n", rcv_src);
    printk(KERN_INFO "dest port:%d\n", dest_port);
    printk(KERN_INFO "destination addr:%s\n", rcv_dst);
    printk(KERN_DEBUG "FLAGS=%c%c%c%c%c%c\n\n",
                    tcp_header->urg ? 'U' : '-',
                    tcp_header->ack ? 'A' : '-',
                    tcp_header->psh ? 'P' : '-',
                    tcp_header->rst ? 'R' : '-',
                    tcp_header->syn ? 'S' : '-',
                    tcp_header->fin ? 'F' : '-');
    printk(KERN_INFO "old ttl:%d\n", ip_header->ttl);

    
    switch (stat)
    {
        case DISCONNECTED:
            handleDisconnectedState(tcp_header, rcv_dst);
            
            //in disconected state we accept every packet
            break;
            
        case CONNECTING:
            handleConnectingState(tcp_header, rcv_dst);
            
            //we accept the ack in connecting state
            break;
            
        case SEND_UNMODIFIED_ACK:
            //I have recvd a fin
            if (tcp_header->fin)
                stat = CLOSING;
    
            //I'am waiting for the ttl from camoTrace
            //all packets are accepted
            //send a shorter ack then turn back to SNE state
            handleSendUnmodifiedAckState(tcp_header, ip_header, rcv_dst, (char *)tcp_header + tcp_hdrlen(skb));

            break;
            
        case SEND_MODIFIED_ACK:
            //send a shorter ack then turn back to SNE state
            handleSendModifiedAckState(tcp_header, ip_header, rcv_dst, (char *)tcp_header + tcp_hdrlen(skb));
            
            break;
            
        case CLOSING:
            handleClosingState(tcp_header, rcv_dst);

            break;
        
    }   
    
    return NF_ACCEPT;           
}



//Called when module loaded using 'insmod'
int init_module()
{    
    targetIP = vmalloc(sizeof(char)*15);
    if (openKernelConnection() < 0)
        return -1;
    
    printk(KERN_INFO "initialize hook handler\n"); 

    //function to call when conditions below met
    output_nfho.hook = outputHook_func;
    input_nfho.hook = inputHook_func;
    //hook for self generated traffic
    output_nfho.hooknum = NF_INET_LOCAL_OUT;  
    input_nfho.hooknum = NF_INET_LOCAL_IN; 
    //IPV4 packets
    output_nfho.pf = PF_INET;     
    input_nfho.pf = PF_INET;    
    //set to highest priority over all other hook functions
    output_nfho.priority = NF_IP_PRI_FIRST; 
    input_nfho.priority = NF_IP_PRI_FIRST;  
    //register hook
    nf_register_net_hook(&init_net, &output_nfho);    
    nf_register_net_hook(&init_net, &input_nfho);                   


    printk(KERN_INFO "STATO = %d\n", stat); 

    return 0; 
}



//Called when module unloaded using 'rmmod'
void cleanup_module()
{
    printk(KERN_INFO "in cleanup\n"); 
    //cleanup – unregister hook
    nf_unregister_net_hook(&init_net, &output_nfho);   
    nf_unregister_net_hook(&init_net, &input_nfho);                   
    
    closeKernelConnection();
   
    printk(KERN_INFO "exiting camoFilter module\n");
}


void closeKernelConnection()
{
    printk(KERN_INFO "release netlink socket\n");
    netlink_kernel_release(nl_sk);   
}


int openKernelConnection()
{
    struct netlink_kernel_cfg cfg = {
	    .groups = 1,
        .input = hello_nl_recv_msg,
    };

    printk("Entering: %s\n", __FUNCTION__);
    memset (targetIP, '\0', sizeof(char) * 15);
    
    
    printk("Initialize netlink socket: %s\n", __FUNCTION__);
    //create the netlink socket
    nl_sk = netlink_kernel_create(&init_net, NETLINK_USER, &cfg);
    if (!nl_sk) {
        printk(KERN_ALERT "Error creating socket.\n");
        return -1;
    }

    return 1;
}