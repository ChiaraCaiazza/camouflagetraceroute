#include "stateHandlers.h"
#include<linux/time.h>

struct timespec lastAckTime;




void handleConnectingState(struct tcphdr *tcp_header, char *pkt_dst)
{
    printk(KERN_INFO "handleConnectingState\n");

    if (tcp_header->ack)
        stat = SEND_UNMODIFIED_ACK;
}



void handleSendUnmodifiedAckState(struct tcphdr *tcp_header, struct iphdr *ip_header, char *pkt_dst, unsigned char *payload)
{

    printk(KERN_INFO "handleUnmodificateAckState\n");
    if (tcp_header->fin)
    {
        stat = CLOSING;
        return;
    }


    if (strlen(payload) > 0 && strstr(payload, "GET / HTTP/1.1") != NULL)
    {
        printk(KERN_INFO "payload -> %s\n", payload);

        //return; 
    }
        
    if (tcp_header->ack)
    {        
        stat = SEND_MODIFIED_ACK;     

        printk(KERN_INFO "\n\nLast ack # = %u\nActual ack # = %u\n", lastAckNumber, ntohl(tcp_header->ack_seq));
        
        if (lastAckNumber >= ntohl(tcp_header->ack_seq))
        {
            replyRcvd = 0;
            printk(KERN_INFO "\n\nReplyRcv = %d", replyRcvd);
        }   

        lastAckNumber = ntohl(tcp_header->ack_seq);
    }
    
    
}



void handleSendModifiedAckState(struct tcphdr *tcp_header, struct iphdr *ip_header, char *pkt_dst, unsigned char *payload)
{
    struct timespec actualTime;

     if (tcp_header->fin)
    {
        stat = CLOSING;
        return;
    }

    if (strlen(payload) > 0 && strstr(payload, "GET / HTTP/1.1") != NULL)
    {
        printk(KERN_INFO "payload -> %s\n", payload);

        return; 
    }

        
    if (tcp_header->ack)
    {
        char ttl_c[5],
             ttl_code[5],
             attempt_number_c[2],
             seqNumber_c[40],
             time_c[200],
             ipID_c[20];

        //unsigned int time_d;

        unsigned long actualTime_l,
                      lastAckTime_l;

        stat = SEND_UNMODIFIED_ACK;


        
        getnstimeofday(&actualTime);
        actualTime_l = (unsigned long)actualTime.tv_sec *1000 +(unsigned long)actualTime.tv_nsec/1000000;
        lastAckTime_l = (unsigned long)lastAckTime.tv_sec *1000 +(unsigned long)lastAckTime.tv_nsec/1000000;

        printk(KERN_INFO "actual time - lastAckTime (ms):%lu\n", (actualTime_l - lastAckTime_l));
        
       // if ((actualTime_l - lastAckTime_l) < 100)
       //     return;
       
        lastAckTime = actualTime;

        memset (ttl_c, '\0', sizeof(char) * 5);
        memset (ttl_code, '\0', sizeof(char) * 5);
        memset (attempt_number_c, '\0', sizeof(char) * 2);
        memset (seqNumber_c, '\0', sizeof(char) * 40);
        memset (time_c, '\0', sizeof(char) * 200);
        memset (ipID_c, '\0', sizeof(char) * 20);

        
        printk(KERN_INFO "\n\nLast ack number # = %u\nActual ack number # = %u\n", lastAckNumber, ntohl(tcp_header->ack_seq));

        if (lastAckNumber >= ntohl(tcp_header->ack_seq))
        {
            replyRcvd = 0;
            printk(KERN_INFO "\n\nReplyRcv = %d", replyRcvd);
        }
        
        lastAckNumber = ntohl(tcp_header->ack_seq);

         //change the ttl
        ip_header->ttl = targetTTL; 

        // compute a new checksum
        ip_header->check = 0;
        ip_send_check(ip_header);
          


        sprintf(ttl_code, "%d", TARGET_TTL_CODE);
        sprintf(ttl_c, "%u", targetTTL);
        sprintf(seqNumber_c, "%u", ntohl(tcp_header -> seq));
        sprintf(attempt_number_c, "%u", actualNAttempt);   
        sprintf(time_c, "%lu", actualTime_l);
        sprintf(ipID_c, "%u", ntohs(ip_header -> id));


        send_kernel_message(ttl_code);
        send_kernel_message(ttl_c);       
        send_kernel_message(seqNumber_c);
        send_kernel_message(attempt_number_c);
        send_kernel_message(time_c);
        send_kernel_message(ipID_c);

        printk(KERN_INFO "new ttl:%s-%s\n", ttl_code, ttl_c);
        printk(KERN_INFO "seq #:%s\n", seqNumber_c);
        printk(KERN_INFO "attempt Number #:%s\n", attempt_number_c);
        printk(KERN_INFO "actual time (ms):%s\n", time_c);
        printk(KERN_INFO "IP ID:%s\n", ipID_c);

        
        

        if (actualNAttempt < N_attempt_max)
            actualNAttempt++;
        else
        {
            actualNAttempt = 1;
            targetTTL++;

            if (targetTTL > ttl_max)
            {
                char msg[2];
                printk(KERN_INFO "stop\n");

                memset (targetIP, '\0', sizeof(char) * 15);
                ttl_max = -1;
                targetTTL = -1;
                N_attempt_max = -1;
                actualNAttempt = -1;
                stat = INITIALIZING;
                replyRcvd = 0;

                sprintf(msg, "%d", STOP_CODE);
                send_kernel_message(msg);
                
            }

        }        
    }  
}



void handleClosingState(struct tcphdr *tcp_header, char *pkt_dst)
{
    printk(KERN_INFO "handleClosingState\n");

    if (tcp_header->ack)
        stat = DISCONNECTED;
    if (tcp_header->syn)
        stat = CONNECTING;
}



void handleDisconnectedState(struct tcphdr *tcp_header, char *pkt_dst)
{
    printk(KERN_INFO "handleDisconnectedState\n");

    if (tcp_header->syn)
        stat = CONNECTING;
}



void handleInitializingState()
{
    char msg[3];

    getnstimeofday(&lastAckTime);
    

    if (targetIP == NULL || ttl_max < 0 || N_attempt_max < 0)
        return;
    

    //send start msg

    printk(KERN_INFO "send start command\n");
    sprintf(msg, "%d\n", START_CODE);
    send_kernel_message(msg);

    targetTTL = 1;
    actualNAttempt = 1;
    stat = DISCONNECTED;
}

