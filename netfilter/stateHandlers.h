#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <net/sock.h>
#include <linux/socket.h>
#include <linux/net.h>
#include <asm/types.h>
#include <linux/netlink.h>
#include <linux/skbuff.h>

#include <linux/sched.h>
#include <linux/string.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/vmalloc.h>
#include <net/ip.h>
#include <net/net_namespace.h>


#include "messageCodes.h"


//camofilter states
#define CONNECTING 0
#define SEND_UNMODIFIED_ACK 1
#define SEND_MODIFIED_ACK 2
#define CLOSING 3
#define DISCONNECTED 4
#define INITIALIZING 5


extern int stat,
           ttl_max,
           N_attempt_max,
           targetTTL,
           actualNAttempt;
extern unsigned int replyRcvd;
//uint32_t lastSeqNumber = 0;
uint32_t lastAckNumber = 0;

extern char *targetIP;




extern void send_kernel_message(char *);


void handleConnectingState(struct tcphdr *tcp_header, char *pkt_dst);
void handleSendUnmodifiedAckState(struct tcphdr *tcp_header, struct iphdr *ip_header, char *pkt_dst, unsigned char *data);
void handleSendModifiedAckState(struct tcphdr *tcp_header, struct iphdr *ip_header, char *pkt_dst, unsigned char *data);
void handleClosingState(struct tcphdr *tcp_header, char *pkt_dst);
void handleDisconnectedState(struct tcphdr *tcp_header, char *pkt_dst);
void handleInitializingState(void);